@if (count($errors) > 0)
    <div class="alert alert-danger">
        @foreach ($errors->all() as $error)
            <span class="fa fa-times"></span> {{ $error }}<br>
        @endforeach
    </div>
@endif


@if(Session::has("message"))
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        {{ Session::get("message") }}
    </div>
@endif


@if(Session::has("messageerror"))
    <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        {{ Session::get("messageerror") }}
    </div>
@endif
