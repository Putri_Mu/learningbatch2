<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\UsersValidasi;
use App\Users;
use Redirect;
use Image;

class UsersController extends Controller
{
    public function index()
    {
        $users = Users::all();
        return view('users.users', compact('users'));
    }


    public function create()
    {
        return view('users.create');
    }

    public function store(UsersValidasi $request)
    {

        $gambar = $request->file('profile');
        $namagambar = 'users-' . date('His') . '.' . $gambar->getClientOriginalExtension();
        Image::make($gambar)->save('images/users/' . $namagambar);

        $tambahDataUsers = users::create(['username' => $request->username, 'password' => bcrypt($request->password), 'nama' => $request->nama, 'profile' => $namagambar, 'created_by' => 1, 'updated_by' => 1]);
        if ($tambahDataUsers) {
            return Redirect::to('users')->with("message", "Berhasil Menambah Data Users ");
        } else {
            return Redirect::to('users')->with("messageerror", "Gagal Menambah Data Users ");
        }

    }

    public function show($id)
    {
        $dataUsers = Users::find($id);
        if (!is_null($dataUsers)) {
            return view('users.detail', compact('dataUsers'));
        }
        return Redirect::to('users')->with("messageerror", "Data Users Tidak Tersedia");
    }

    public function edit($id)
    {
        $dataUsers = Users::find($id);
        if (!is_null($dataUsers)) {
            return view('users.edit', compact('dataUsers'));
        }
        return Redirect::to('users')->with("messageerror", "Data Users Tidak Tersedia");

    }


    public function update(UsersValidasi $request, $id)
    {
        $dataUsers = Users::find($id);
        if (!is_null($dataUsers)) {
            if ($request->file('profile')) {
                $gambar = $request->file('profile');
                $namagambar = 'users-' . date('His') . '.' . $gambar->getClientOriginalExtension();
                Image::make($gambar)->save('images/users/' . $namagambar);
            } else {
                $namagambar = $dataUsers->gambar;
            }

            if($request->password==""){
                $password = $dataUsers->password;
            }else{
                $password = bcrypt($request->password);
            }

            $dataUsers->update(['username' => $request->username, 'password' => $password, 'nama' => $request->nama, 'profile' => $namagambar, 'updated_by' => 1]);
            return Redirect::to("users")->with("message", "Berhasil Mengubah Data Users ");

        }
        return Redirect::to('users')->with("messageerror", "Data Users Tidak Tersedia");

    }

    public function destroy($id)
    {
        $dataUsers = Users::destroy($id);
        if ($dataUsers) {
            return Redirect::to('users')->with("message", "Berhasil Menghapus Data Users ");
        } else {
            return Redirect::to('users')->with("messageerror", "Gagal Menghapus Data Users ");
        }
    }
}
